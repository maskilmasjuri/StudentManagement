package com.dxc.StudentManagement.service;

import java.util.List;

import com.dxc.StudentManagement.entity.Student;

public interface StudentService {
	List<Student> getAllStudents();
	Student addStudent(Student student);
	Student getStudentById(Long id);
	Student updateStudent(Student student);
	void deleteStudent(Long id);
}
