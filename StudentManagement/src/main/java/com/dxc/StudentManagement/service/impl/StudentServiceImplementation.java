package com.dxc.StudentManagement.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.dxc.StudentManagement.entity.Student;
import com.dxc.StudentManagement.repository.StudentRepository;
import com.dxc.StudentManagement.service.StudentService;

@Service
public class StudentServiceImplementation implements StudentService{
	private StudentRepository studentRepo;
	
	public StudentServiceImplementation(StudentRepository studentRepo) {
		super();
		this.studentRepo = studentRepo;
	}
	
	@Override
	public List<Student> getAllStudents() {
		return studentRepo.findAll();
	}

	@Override
	public Student addStudent(Student student) {
		return studentRepo.save(student);
	}

	@Override
	public Student getStudentById(Long id) {
		return studentRepo.findById(id).get();
	}

	@Override
	public Student updateStudent(Student student) {
		return studentRepo.save(student);
	}

	@Override
	public void deleteStudent(Long id) {
		studentRepo.deleteById(id);
	}

}
