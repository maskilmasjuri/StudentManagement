package com.dxc.StudentManagement.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.dxc.StudentManagement.entity.Student;
import com.dxc.StudentManagement.service.StudentService;

@Controller
public class StudentManagementController {
	private StudentService studentService;
	
	public StudentManagementController(StudentService studentService) {
		super();
		this.studentService = studentService;
	}
	
	//Handler method to handle students list, returns model and view
	@GetMapping("/students")
	public String listAllStudents(Model model) {
		model.addAttribute("students", studentService.getAllStudents());
		return "students";
	}
	
	//Create Student Object to store student form data
	@GetMapping("/students/new")
	public String createStudentForm(Model model) {
		Student student = new Student();
		model.addAttribute("student", student);
		return "create_student";
	}
	
	//Handler method to add new student
	@PostMapping("/students")
	public String addStudent(@ModelAttribute("student") Student student) {
		studentService.addStudent(student);
		return "redirect:/students";
	}
	
	//Get student by id to edit data
	@GetMapping("/students/edit/{id}")
	public String editStudentForm(@PathVariable Long id, Model model) {
		model.addAttribute("student", studentService.getStudentById(id));
		return "edit_student";
	}
	
	//Handler method to edit student
	
}
